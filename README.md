# README #

Blackboard Treebank is a Thai dependency bank based on the LST20 Annotation Guideline. 
It features dependency structures, constituency structures, word boundaries, named entities, 
clause boundaries, and sentence boundaries. It is available in the CoNLL-U format for 
universal compatibility.

![Example](https://bitbucket.org/kaamanita/blackboard-treebank/raw/e8d2b559927999e2d549837de72d2d070e40824d/guidelines/example_blackboard_treebank.png)

For more information, the introduction slides are provided in the file
`Blackboard-Treebank.pdf`.

## Annotation Guideline

The annotation guideline can be found in the folder `guidelines`.

## Format ##

Blackboard Treebank complies with the CoNLL-U format, where

- Each word of a sentence is annotated as a line consisting of ten columns separated by a single tab character.
- Blank lines mark the clause boundaries.
- Comment lines start with a hash symbol `#`.

The ten fields for each word line are described below:

1. ID: word index
2. FORM: surface form a word or a chunk of words (separated by `|`)
3. LEMMA: lemma of a word or a chunk of lemmata (separated by `|`)
4. POS: POS tag (LST20)
5. XPOS: POS tag or a POS-tag sequence
6. FEATS: Morphological features
7. HEAD: Head of the current word
8. DEPREL: Dependency relation
9. DEPS: Head-deprel pairs (always `_` in Blackboard Treebank)
10. MISC: Syntactic categories and NE-tag sequence

## Versioning

- AUG 10, 2021: Public alpha version -- **USE WITH CAUTIONS**
    - AUG 17, 2021: Weekly update. The number of clauses has been increased to 132,001.
    - AUG 25, 2021: Weekly update. The serial verbs and serial nouns are expanded into words. The number of clauses are now 130,561, and the number of words is 982,893.
- Late-SEP 2021: Public beta version -- **USE WITH CAUTIONS**
- Late-OCT 2021: Full release version

## Contact ##

This repository is maintained by Prachya Boonkwan, NECTEC Thailand.
